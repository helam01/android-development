package com.example.k19_intents_actions;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btn_viewSite = (Button) findViewById(R.id.view_site_btn);
		Button btn_emailSite = (Button) findViewById(R.id.view_email_btn);
		Button btn_callSite = (Button) findViewById(R.id.view_call_btn);
		
		btn_viewSite.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW,
						Uri.parse("http://tecmundo.com.br"));
				startActivity(intent);
			}
		});
		
		
		btn_emailSite.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("plain/text");
				intent.putExtra(Intent.EXTRA_EMAIL, 
						new String[]{"helam.01@hotmail.com"});
				startActivity(Intent.createChooser(intent, "Enviar email"));
			}
		});
		
		btn_callSite.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:*545#"));
				startActivity(intent);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
