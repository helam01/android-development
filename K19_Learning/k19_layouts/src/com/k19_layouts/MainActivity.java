package com.k19_layouts;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.linear);
		
		final EditText editedText = (EditText) findViewById(R.id.fld_editText);
		Button btnShowMsg = (Button) findViewById(R.id.btn_showMsg);
		final TextView showTextMsg = (TextView) findViewById(R.id.txt_msg);
		
		btnShowMsg.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				String text = editedText.getEditableText().toString();
				showTextMsg.setText(text);
				showTextMsg.setVisibility(View.VISIBLE);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
