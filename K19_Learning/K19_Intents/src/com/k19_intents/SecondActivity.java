package com.k19_intents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
		
		// Pega os dados passados pela Intent
		Bundle extras = getIntent().getExtras();
		String extra_email = extras.getString("str_email");
		String extra_name = extras.getString("str_name");
		
		// Pega os objetos dos elementos da p�gina (TextView)
		final TextView tw_name = (TextView) findViewById(R.id.txt_name);
		final TextView tw_email = (TextView) findViewById(R.id.txt_email);
		
		tw_name.setText("Name: " + extra_name);
		tw_email.setText("Email: " + extra_email);
		
		Button btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent_back = new Intent(SecondActivity.this, MainActivity.class);
				startActivity(intent_back);
			}
			
		});
	}
}
