package com.k19_intents;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		//Pega os objetos dos elementos da pagina
		final EditText edit_name = (EditText) findViewById(R.id.edit_name);
		final EditText edit_email = (EditText) findViewById(R.id.edit_email);
		
		
		Button button = (Button) findViewById(R.id.main_button);
		button.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, SecondActivity.class);
				intent.putExtra("str_name", edit_name.getEditableText().toString());
				intent.putExtra("str_email", edit_email.getEditableText().toString());				
				
				startActivity(intent);
			}
			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
